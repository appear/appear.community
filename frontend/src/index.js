import React from 'react';
import ReactDOM from 'react-dom';
import App from 'containers/app/App';
import { BrowserRouter } from 'react-router-dom';
import { AppContainer } from 'react-hot-loader';
import 'antd/dist/antd.min.css';
import registerServiceWorker from './registerServiceWorker';

const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
      <BrowserRouter>
        <Component />
      </BrowserRouter>
    </AppContainer>,
    document.getElementById('root'),
  );
};

render(App);

if (module.hot) {
  module.hot.accept('containers/app/App', () => {
    render(App);
  });
}

registerServiceWorker();
