import React from 'react';
import { Layout, Menu, Row, Col } from 'antd';
import { Link } from 'react-router-dom';
import './navigation.css';

const { Header } = Layout;

const makeList = (key, title, href) => (
  <Menu.Item key={key}>
    <Link to={href}>{title}</Link>
  </Menu.Item>
);

// TODO LINK에 따라 default select
const Navigation = () => (
  <Header
    className="wrap_navigation"
    style={{
      position: 'fixed',
      width: '100%',
      padding: '0 10px',
      backgroundColor: '#fff',
    }}
  >
    <Row gutter={8}>
      <Col span={7}>
        <h1>이야기</h1>
      </Col>
      <Col span={17}>
        <Menu
          className="list_menu"
          theme="light"
          mode="horizontal"
          defaultSelectedKeys={['1']}
          style={{ lineHeight: '50px' }}
        >
          {makeList('1', '메인', '/')}
          {makeList('2', '질문하기', '/writeboard')}
          {makeList('3', '로그인', '/login')}
        </Menu>
      </Col>
    </Row>
  </Header>
);

export default Navigation;
