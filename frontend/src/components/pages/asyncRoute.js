import asyncRoute from 'lib/AsyncComponent';

const ConHome = asyncRoute(() => import('containers/home/ConHome'));
const ConRegisterPost = asyncRoute(() => import('containers/registerPost/ConRegisterPost'));

export { ConHome, ConRegisterPost };
