// @flow

import React from 'react';
import { Layout } from 'antd';
import { Route, Switch } from 'react-router-dom';
/* Layout */
import { Navigation } from 'components/base';
import { ConHome, ConRegisterPost } from 'components/pages';

import './App.css';

const { Content } = Layout;

// async component
const App = () => (
  <Layout
    className="wrap_root"
    style={{ background: '#fff', maxWidth: '1000px', margin: '0 auto' }}
  >
    <Navigation />
    <Content className="wrap_body" style={{ padding: '0 10px', marginTop: 80 }}>
      <Switch>
        <Route exact path="/" component={ConHome} />
        <Route path="/writeBoard" component={ConRegisterPost} />
      </Switch>
    </Content>
  </Layout>
);

export default App;
