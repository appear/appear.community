import React, { Component } from 'react';
import ReactMde from 'react-mde';
import { Popover, Row, Col, Icon, Input, Radio, Select } from 'antd';
import * as Showdown from 'showdown';
import 'react-mde/lib/styles/css/react-mde-all.css';
import './registerPost.css';

class WriteBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mdeState: null,
    };
    this.converter = new Showdown.Converter({
      tables: true,
      simplifiedAutoLink: true,
      strikethrough: true,
      tasklists: true,
    });
  }

  handleValueChange = (mdeState) => {
    this.setState({ mdeState });
  };

  handleChange = (value) => {
    console.log(`selected ${value}`);
  };
  render() {
    const RadioGroup = Radio.Group;
    const Options = Select.Option;

    const markdownDesc = (
      <div>
        <p># ~ ######: H1 ~ H2 </p>
        <p>[Text](URL): Link</p>
        <p>```CODE```: 코드</p>
        <p>* or -: 리스트</p>
        <p>**Text**: 강조</p>
        <p>space 3번: 줄바꿈</p>
      </div>
    );
    const arr = [
      <Options key="a">a</Options>,
      <Options key="b">b</Options>,
      <Options key="c">c</Options>,
    ];

    // TODO Redux로 기기판단 필요
    return (
      <div className="wrap_writeboard" style={{ background: '#fff' }}>
        <Row gutter={24} className="cont_survey">
          <Row gutter={24} className="cont">
            <Col span={24} className="tit_cont">
              제목
            </Col>
            <Col span={24}>
              <Input
                placeholder="글 제목을 작성해주세요 :)"
                prefix={<Icon type="edit" style={{ color: 'rgba(0,0,0,.25)' }} />}
              />
            </Col>
          </Row>
          <Row gutter={24} className="cont">
            <Col span={24} className="tit_cont">
              성별을 알려주세요
            </Col>
            <Col span={24}>
              <RadioGroup name="radiogroup" defaultValue={1}>
                <Radio value={1}>남자</Radio>
                <Radio value={2}>여자</Radio>
              </RadioGroup>
            </Col>
          </Row>
          <Row gutter={24} className="cont">
            <Col span={24} className="tit_cont">
              연령대{' '}
              <span className="txt_sub">
                (더 좋은 블로그를 만들기 위해 간단한 통계자료를 받고있습니다. 도와주세요..!)
              </span>
            </Col>
            <Col span={24}>
              <RadioGroup name="radiogroup" defaultValue={1}>
                <Radio value={1}>10대</Radio>
                <Radio value={2}>20대</Radio>
                <Radio value={3}>30대</Radio>
                <Radio value={4}>40대</Radio>
              </RadioGroup>
            </Col>
          </Row>
          <Row gutter={24} className="cont">
            <Col span={24} className="tit_cont">
              관심사
            </Col>
            <Col span={24}>
              <Select
                mode="tags"
                style={{ width: '100%' }}
                placeholder="요즘 관심사를 적어주세요 (생략가능)"
                onChange={this.handleChange}
              >
                {arr}
              </Select>
            </Col>
          </Row>

          <Row gutter={24} className="cont">
            <Col span={24} className="tit_cont">
              당신은 누구신가요?
            </Col>
            <Col span={24}>
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="당신은 누구십니까 :)"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                <Options key="프론트엔드 개발자" value="프론트엔드 개발자">
                  백엔드 개발자
                </Options>
                <Options key="백엔드 개발자" value="백엔드 개발자">
                  프론트엔드 개발자
                </Options>
                <Options key="학생" value="학생">
                  학생
                </Options>
                <Options key="비전공자" value="비전공자">
                  비전공자
                </Options>
                <Options key="기타" value="기타">
                  기타
                </Options>
              </Select>
            </Col>
          </Row>
        </Row>

        <Row gutter={16} className="wrap_help">
          <Col span={4}>글작성</Col>
          <Col span={20}>
            <div className="cont_help">
              <span className="txt_help">혹시 Markdown 문법을 모르시나요?</span>
              <Popover
                placement="bottomRight"
                title="MarkDown 문법"
                content={markdownDesc}
                trigger="click"
              >
                <Icon type="question-circle" />
              </Popover>
            </div>
          </Col>
        </Row>
        <ReactMde
          layout="horizontal"
          onChange={this.handleValueChange}
          editorState={this.state.mdeState}
          generateMarkdownPreview={markdown => Promise.resolve(this.converter.makeHtml(markdown))}
        />
      </div>
    );
  }
}

export default WriteBoard;
