import React, { Component } from 'react';
import { Carousel } from 'antd';
import './home.css';

class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div style={{ background: '#fff', minHeight: 380 }}>
        <Carousel autoplay>
          <div>
            <h3>아령</h3>
          </div>
          <div>
            <h3>하세여</h3>
          </div>
          <div>
            <h3>이야기</h3>
          </div>
          <div>
            <h3>나눠여</h3>
          </div>
        </Carousel>
      </div>
    );
  }
}

export default MainPage;
