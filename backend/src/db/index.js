const Sequelize = require('sequelize');
const { db } = require('../../config/config');

// dbname, user, pw
const database = new Sequelize(db.table, db.user, db.password, {
  host: db.host,
  dialect: 'mysql',
  logging: true,
  define: {
    underscored: true,
  },
});

module.exports = database;
