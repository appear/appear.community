const { Post, Survey } = require('./models');

function associate() {
  Survey.associate();
}

const sync = () => {
  associate();
  Post.sync();
  Survey.sync();
};

module.exports = { associate, sync };
