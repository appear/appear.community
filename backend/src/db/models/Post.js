const Sequelize = require('sequelize');
const db = require('../');

const Post = db.define(
  'Post',
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true,
    },
    title: Sequelize.STRING,
    author: Sequelize.STRING,
    view_count: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
    },
    create_date: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
    update_date: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
    },
    content: Sequelize.TEXT,
    isSecret: Sequelize.BOOLEAN,
  },
  {
    tableName: 'post',
    timestamps: true, // create, update date를 찍어준다
    paranoid: true, // 데이터가 삭제된 것 처럼 보여준다
  },
);

Post.insert = function (params) {
  return Post.build(params).save();
};

Post.prototype.show = function () {
  return this.increment('view_count', { by: 1 });
};

module.exports = Post;
