const Sequelize = require('sequelize');
const db = require('../');
const Post = require('./Post');

const Servey = db.define(
  'Survey',
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true,
    },
    fk_post_id: Sequelize.UUID,
    age: Sequelize.INTEGER,
    job: Sequelize.STRING,
    interest: Sequelize.STRING,
  },
  {
    tableName: 'survey',
    timestamps: true, // create, update date를 찍어준다
    paranoid: true, // 데이터가 삭제된 것 처럼 보여준다
  },
);

Servey.associate = function associate() {
  Servey.belongsTo(Post, {
    foreignKey: 'fk_post_id',
    onDelete: 'restrict',
    onUpdate: 'restrict',
  });
};

Servey.insert = function (params) {
  return Servey.build(params).save();
};

module.exports = Servey;
