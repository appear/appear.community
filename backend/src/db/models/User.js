const Sequelize = require('sequelize');
const db = require('../');

const User = db.define('User', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
  },
  name: {
    tableName: 'user',
    type: Sequelize.STRING,
    allowNull: true,
  },
});

module.exports = User;
