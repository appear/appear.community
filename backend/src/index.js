const App = require('./app');
const { server } = require('../config/config');

const port = process.env.port || server.port;
const { app } = new App();

/* Dev server */
// const WebpackDevServer = require('webpack-dev-server');
// const webpack = require('webpack');
// const webpackConfig = require('../../frontend/webpack.dev.config');

// if (process.env.NODE_ENV === 'development') {
//   console.log('Server is running on dev mode');

//   const compiler = webpack(webpackConfig);

//   const devServer = new WebpackDevServer(compiler, webpackConfig.devServer);
//   devServer.listen(server.devPort, () => {
//     console.log('webpack-dev-server is listening on port', server.devPort);
//   });
// }

app.listen(port, () => console.log(`Express Server on.... running port: ${port}`));
