const Joi = require('joi');

const validateSchema = (params, schema) => {
  const result = Joi.validate(params, schema);
  return !result.error;
};

module.exports = {
  validateSchema,
};
