global.Message = function (status, code, message, response) {
  this.status = !!status;
  this.code = code || '';
  this.message = message || '';
  this.response = response || {};
};

global.MessageErr = function (e, code, message) {
  const errCode = code || 'ERROR0000';
  return e instanceof Message
    ? e
    : new Message(false, errCode, `처리오류 - ${message || ''}${e.stack.substr(0, 2000)}`);
};
