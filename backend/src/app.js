const Koa = require('koa');
const db = require('./db');
const { associate } = require('./db/sync');
const router = require('./router');
const serve = require('koa-static');
const views = require('koa-render-view');
const path = require('path');

require('./lib/messageMaker');

class App {
  constructor() {
    this.app = new Koa();
    this.middleware();
    this.initializeDb();
  }

  initializeDb() {
    db.authenticate().then(
      () => {
        associate();
        console.log('DB Connection....');
      },
      (err) => {
        console.error('Unable to connect to the DB:', err);
      },
    );
  }

  middleware() {
    const { app } = this;
    const publicPath = path.join(__dirname, '../../frontend/build');

    // TODO allowedMethods 에러처리 알아보기
    app.use(router.routes());
    app.use(serve(publicPath));
    app.use(views(path.join(__dirname, publicPath)));
  }
}

module.exports = App;
