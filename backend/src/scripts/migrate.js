const db = require('../db');
const { sync } = require('../db/sync');

const migrate = async () => {
  await db.authenticate();
  sync();
};

migrate();
