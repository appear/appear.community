const Router = require('koa-json-router');

const router = new Router();

const post = require('./post');

router.use('*', (ctx) => {
  ctx.body = 'Hello World';
});

router.use('/post', post.routes());

module.exports = router;
