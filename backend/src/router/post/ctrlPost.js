const postModel = require('../../db/models/Post');
const survey = require('../../db/models/Survey');
const Joi = require('joi');
const { validateSchema } = require('../../lib/validate');

const writePost = async (ctx) => {
  const { post, survey } = ctx.body;

  // validation Schema
  const postSchema = Joi.object().keys({
    title: Joi.string()
      .min(1)
      .max(120)
      .required(),
    author: Joi.string()
      .min(1)
      .max(50)
      .required(),
    content: Joi.string()
      .min(1)
      .required(),
    isSecret: Joi.boolean().required(),
  });

  const surveySchema = Joi.object().keys({
    fk_post_id: Joi.required(),
  });

  // validation
  if (!validateSchema(post, postSchema) || !validateSchema(survey, surveySchema)) {
    return MessageErr(e, 'ERROR00001', 'POST 등록정보를 확인해주세요');
  }

  try {
    const post = await postModel.insert(post);
    const survery = await survey.insert(Object.assign({}, survey, {
      fk_post_id: post.id,
    }));
    return new Message(true, 'SUCCESS0000', 'POST 등록에 성공했습니다');
  } catch (e) {
    return MessageErr(e, 'ERROR00000', 'POST 등록에 실패했습니다.');
  }
};

module.exports = {
  writePost,
};
