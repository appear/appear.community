const Router = require('koa-json-router');

const post = new Router();
const postCtrl = require('./ctrlPost');

post.post('/', postCtrl.writePost);

module.exports = post;
