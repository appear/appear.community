const Router = require('koa-json-router');

const auth = new Router();
const ctrlAuth = require('./ctrlAuth');

auth.post('/register', ctrlAuth.register);
auth.post('/login', ctrlAuth.login);
